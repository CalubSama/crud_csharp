﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CrudCatalogClients.logic
{
    public enum SearchOperator
    {
        AND,
        OR,
        LIKE,
        EQUAL,
        DIFFERENT,
        NO_EQUAL,
        NONE
    }
}
