﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CrudCatalogClients.logic
{
    class UpdateData
    {
        public string field;
        public string val;

        public UpdateData(string field, string val)
        {
            this.field = field;
            this.val = val;
        }

        public string Field { get => field; set => field = value; }
        public string Val { get => val; set => val = value; }
    }

}

