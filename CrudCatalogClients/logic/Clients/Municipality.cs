﻿using data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CrudCatalogClients.logic.Clients
{
    class Municipality
    {
        MySqlDB db;
        private int id;
        private int fk_state_id;
        private string municipa;

        public Municipality()
        {
            db = new MySqlDB("127.0.0.1", "root", "jctd", "crud_clients_db");
        }

        //--------------------------------------------*********************************************----------------------------------------------------
        /// <summary>
        /// Search municipalities
        /// </summary>
        public List<Municipality> Search(int idState)
        {
            string where = "fk_state_id = " + idState ;
            List<object> listTemp = db.Consult("*", "municipalities", where);
            List<Municipality> listMunicp = new List<Municipality>();
            foreach (var registry in listTemp)
            {
                    object[] array = (object[])registry;
                    Municipality temp = new Municipality();
                    temp.Id = int.Parse(array[0].ToString());
                    temp.Fk_state_id = int.Parse(array[1].ToString());
                    temp.Municipa = array[2].ToString();
                    listMunicp.Add(temp);
            }
            return listMunicp;
        }


        public int Id { get => id; set => id = value; }
        public int Fk_state_id { get => fk_state_id; set => fk_state_id = value; }
        public string Municipa { get => municipa; set => municipa = value; }
    }
}
