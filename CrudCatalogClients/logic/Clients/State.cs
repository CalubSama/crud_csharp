﻿using data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CrudCatalogClients.logic.Clients
{
    class State
    {
        MySqlDB db;
        private int id;
        private string stat;

        public State()
        {
            db = new MySqlDB("127.0.0.1", "root", "jctd", "crud_clients_db");
        }


        //--------------------------------------------*********************************************----------------------------------------------------
        /// <summary>
        /// Search states
        /// </summary>
    
            
            
        public List<State> Search()
        {
            string where = "id > 0";
            List<object> listTemp = db.Consult("*", "states", where);
            List<State> listStates = new List<State>();

            foreach ( var registry in listTemp)
            {
                object[] array = (object[])registry;
                    State temp = new State();
                    temp.Id = int.Parse(array[0].ToString());
                    temp.Stat = array[1].ToString();
                    listStates.Add(temp);
            }
            return listStates;
        }


        public int Id { get => id; set => id = value; }
        public string Stat { get => stat; set => stat = value; }


    }
}
