﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using data;

namespace CrudCatalogClients.logic
{
    class Client: ICrud
    {

        public static string msgError;
        //Vars 
       private int id;
       private string name;
       private string RFC;
       private string state;
       private string municipality;
        //instance of db
        MySqlDB db;

        //constructor
        public Client()
        {
            db = new MySqlDB("127.0.0.1", "root", "jctd", "crud_clients_db");
        }


        //--------------------------------------------*********************************************----------------------------------------------------
        /// <summary>
        /// create query for this table
        /// </summary>
        /// <param name="nam">client name</param>
        /// <param name="rfc">client rfc</param>
        /// <param name="stat">client state</param>
        /// <param name="municip">client municipality</param>
        public bool Insert(string name, string rfc, string stat, string municip)
        {
            bool res = false;
            string values = "'" + name + "','" + rfc + "','" + stat + "','" + municip + "'";
            if (db.Insert("clients",
                            "name, RFC, fk_id_state, fk_id_municipality",
                            values))
            {
                res = true;
            }
            else
            {
                msgError = "Error to create new client. " + MySqlDB.msgError;
            }
            return res;
        }


        //--------------------------------------------*********************************************----------------------------------------------------
        /// <summary>
        /// create new client
        /// </summary>
        public bool Add()
        {
            return Insert(this.Name, this.RFC1, this.State, this.Municipality);
        }

        //--------------------------------------------*********************************************----------------------------------------------------
        //search a client using params 
        /// <summary>
        /// Search CLients using params, like name or rfc
        /// </summary>
        /// <<param name="SearchParams">params</param>
        public List<object> Search(List<SearchParams> SearchParams)
        {
            List<object> res = new List<object>();
            string where = "";
            for (int i = 0; i < SearchParams.Count; i++)
            {
                string middleOp = "";
                switch (SearchParams[i].middleOperator)
                {
                    case SearchOperator.EQUAL: middleOp = "="; break;
                    case SearchOperator.LIKE: middleOp = "LIKE"; break;
                    case SearchOperator.DIFFERENT: middleOp = "<>"; break;
                    case SearchOperator.NO_EQUAL: middleOp = "!="; break;
                    default: middleOp = ""; break;
                }
                string finalOp = "";
                switch (SearchParams[i].finalOperator)
                {
                    case SearchOperator.AND: finalOp = "AND"; break;
                    case SearchOperator.OR: finalOp = "OR"; break;

                    default: finalOp = ""; break;
                }
                where += " " + SearchParams[i].field + " " + middleOp + " " + SearchParams[i].value + " " + finalOp + " ";
            }
            //for to create where
            //do te query
            List<object> tmp = db.Consult("*", " clients ", where);
            //map each Object in a Client
            foreach (object[] cliTmp in tmp)
            {
                var cli = new
                {
                    Id = int.Parse(cliTmp[0].ToString()),
                    Name = cliTmp[1].ToString(),
                    RFC = cliTmp[2].ToString(),
                    State = cliTmp[3].ToString(),
                    Municipality= cliTmp[4].ToString(),
                };
                res.Add(cli);
            }
            //Return list client
            return res;
        }

        //--------------------------------------------*********************************************----------------------------------------------------
        //TODO ---Refactor--- this method
        //take the data of the 3 tables to all info of the client
        public List<object> SearchCli()
        {
            List<object> res = new List<object>();
            List<object> tmp = db.SearchCli();
            //map each Object in a Client
            foreach (object[] cliTmp in tmp)
            {
                var cli = new
                {
                 /*   Id = int.Parse(cliTmp[0].ToString()),*/
                    Id = cliTmp[0].ToString(),
                    Name = cliTmp[1].ToString(),
                    RFC = cliTmp[2].ToString(),
                    State = cliTmp[3].ToString(),
                    Municipality = cliTmp[4].ToString(),
                };
                res.Add(cli);
            }
            //Return list client
            return res;
        }

        //--------------------------------------------*********************************************----------------------------------------------------
        /// <summary>
        /// Update client
        /// </summary>
        /// <<param name="data">data to update</param>
        /// <<param name="id">id of the client</param>
        public bool Update(List<UpdateData> data, int id)
        {
            //create data
            string fieldsValues = "";
            for (int i = 0; i < data.Count; i++)
            {
                fieldsValues += " " + data[i].field + " = " + "'" + data[i].val + "'";
                if (i < data.Count - 1) fieldsValues += ",";
            }
            return db.Update("clients", fieldsValues, "id=" + id);
        }

        //--------------------------------------------*********************************************----------------------------------------------------
        /// <summary>
        /// Delete register
        /// </summary>
        /// <param name="id">id client</param>
        public bool Delete(int id)
        {
            return db.Delete("clients", " id=" + id);
        }





        //--------------------------------------------*********************************************----------------------------------------------------
        //Getters and Setters
        public int Id { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public string RFC1 { get => RFC; set => RFC = value; }
        public string State { get => state; set => state = value; }
        public string Municipality { get => municipality; set => municipality = value; }
    }
}
