﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CrudCatalogClients.logic
{
    interface ICrud
    {
        /// <summary>
        /// Insert each type of obj
        /// </summary>
        /// <returns>true if haven´t error, flase if have error</returns>
        bool Add();


        /// <summary>
        /// UPDATE of each type obj
        /// </summary>
        /// <param name="data">field=value</param>
        /// <param name="id">Id of registery to update</param>
        /// <returns>true if haven´t error, flase if have error</returns>
        bool Update(List<UpdateData> data, int id);


        /// <summary>
        /// Delete registery with ID
        /// </summary>
        /// <param name="id">Id of register to delete</param>
        /// <returns>true if haven´t error, flase if have error</returns>
        bool Delete(int id);


        /// <summary>
        /// SELECT 
        /// </summary>
        /// <typeparam name="T">Type of objetc to consult</typeparam>
        /// <param name="searchParams">List of obj which have field, value and operator</param>
        /// <returns>list of types obj which are definited like 'T'</returns>
        List<object> Search(List<SearchParams> searchParams);

    }
}
