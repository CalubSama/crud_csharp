﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace CrudCatalogClients.logic.Regexp
{
    class Validation
    {
        /// <summary>
        /// Validate personal name
        /// </summary>
        /// <param name="cad">string to validate</param>
        /// <returns>true if it´s fine, false if don´t match</returns>
        public static bool PersonalName(string cad)
        {
            return Regex.Match(cad, @"^[A-Z][a-zA-ZÀ-ÿ\u00f1\u00d1]+\s?([A-Z][a-zA-ZÀ-ÿ\u00f1\u00d1]+\s?)*$") != Match.Empty;
        }

        /// <summary>
        /// Validate RFC
        /// </summary>
        /// <param name="cad">String to valdiate</param>
        /// <returns>true if it´s fine, false if don´t match</returns>
        public static bool ValRFC(string cad)
        {
            return Regex.Match(cad, @"^([A-Z\s]{4})\d{6}([A-Z\w]{3})$") != Match.Empty;
        }

        /// <summary>
        /// Validate Email
        /// </summary>
        /// <param name="cad">string to validate</param>
        /// <returns>true if t´s fine, false if don´t match</returns>
        public static bool Email(string cad)
        {
            return Regex.Match(cad, @"^[a-z][a-z0-9]{3,18}@([a-z][a-z0-9]{3,18}(\.[a-z]{3,4})+)$") != Match.Empty;
        }

        /// <summary>
        /// Validate postal code
        /// </summary>
        /// <param name="cad">string to validate</param>
        /// <returns>true if t´s fine, false if don´t match</returns>
        public static bool PostalCode(string cad)
        {
            return Regex.Match(cad, @"^83[0-9]{3}$") != Match.Empty;
        }

        /// <summary>
        /// Valide phone number
        /// </summary>
        /// <param name="cad">string to validate</param>
        /// <returns>true if t´s fine, false if don´t match</returns>
        public static bool Phone(string cad)
        {
            //+52 6622001809
            return Regex.Match(cad, @"^[0-9]{2,3}[0-9]{7,8}$") != Match.Empty;
        }

    }
}
