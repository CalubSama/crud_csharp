﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CrudCatalogClients.logic
{
    class SearchParams
    {
        public string field;
        public SearchOperator middleOperator;
        public string value;
        public SearchOperator finalOperator;

        /// <summary>
        /// query consult of operator
        /// </summary>
        /// <returns>String of the operator in use (=, LIKE, menorMayor, !=)</returns>
        public string middleOpSql()
        {
            string res = "";
            switch (middleOperator)
            {
                case SearchOperator.EQUAL: res = "="; break;
                case SearchOperator.LIKE: res = "LIKE"; break;
                case SearchOperator.DIFFERENT: res = "<>"; break;
                case SearchOperator.NO_EQUAL: res = "!="; break;
                default: res = ""; break;
            }
            return res;
        }

        /// <summary>
        /// Add "and" or "OR" to te query 
        /// </summary>
        /// <returns>AND, OR o "" </returns>
        public string opFinalSql()
        {
            string res = "";
            switch (this.finalOperator)
            {
                case SearchOperator.AND: res = "AND"; break;
                case SearchOperator.OR: res = "OR"; break;
                default: res = ""; break;
            }
            return res;
        }
    }
}
