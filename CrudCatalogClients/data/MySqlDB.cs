﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;


namespace data
{
    public class MySqlDB
    {

        //objs of DB
        MySqlConnection con;
        //command
        MySqlCommand com;
        //data reader
        MySqlDataReader dr;
        MySqlDataAdapter da;
        //Vars of conection
        string server = "";
        string us = "";
        string pwd = "";
        string bd = "";

        public static string msgError = "";

        //--------------------------------------------*********************************************----------------------------------------------------
        /// <summary>
        /// Conexión por default  
        /// delete this at final of develop...
        /// </summary>
        public MySqlDB()
        {
            this.server = "127.0.0.1";
            this.us = "root";
            this.pwd = "jctd";
            this.bd = "crud_clients_db";
            // ConnectionString
            string connectionString = "SERVER=" + server +
                                    "; UID=" + us +
                                    "; PWD=" + pwd +
                                    "; DATABASE=" + bd + ";";
            //instance of connection
            con = new MySqlConnection(connectionString);

        }

        //--------------------------------------------*********************************************----------------------------------------------------
        /// <summary>
        /// Constructor on detail
        /// </summary>
        /// <param name="server">server ip to connect (localhost 0 127.0.0.1 )</param>
        /// <param name="us">user of the db in use 'root'.</param>
        /// <param name="pwd">user password</param>
        /// <param name="bd">name of database to conect</param>
        public MySqlDB(string server, string us, string pwd, string bd)
        {
            //encapsular los vars
            this.server = server;
            this.us = us;
            this.pwd = pwd;
            this.bd = bd;
            //generate of ConnectionString
            string connectionString = "SERVER=" + server +
                                    "; UID=" + us +
                                    "; PWD=" + pwd +
                                    "; DATABASE=" + bd + ";";
            //instance of dbconexion
            con = new MySqlConnection(connectionString);
        }

        //--------------------------------------------*********************************************----------------------------------------------------
        /// <summary>
        /// Open connection
        /// </summary>
        /// <returns>true if is open, false if can´t open</returns>
        private bool openConnection()
        {
            bool res = false;
            try
            {
                //verificar la conex
                if (con.State != System.Data.ConnectionState.Open)
                    con.Open();
                //abrir la conex
                res = true;
            }
            catch (MySqlException sqlExcept)
            {
                msgError = "Error trying to open connection,with error: " + sqlExcept.Message;
            }
            return res;
        }

        //--------------------------------------------*********************************************----------------------------------------------------
        /// <summary>
        /// close connection
        /// </summary>
        /// <returns>true if the connection is close, false if can´t close the connection</returns>
        private bool closeConnection()
        {
            bool res = false;
            try
            {
                //verify connection
                if (con.State != System.Data.ConnectionState.Closed)
                    con.Close();
                //abrir la conex
                res = true;
            }
            catch (MySqlException sqlExcept)
            {
                msgError = "Error closing connection,with error:  " + sqlExcept.Message;
            }
            return res;
        }

        //--------------------------------------------*********************************************----------------------------------------------------
        /// <summary>
        /// insert data to the Database
        /// </summary>
        /// <param name="table">name of the database table</param>
        /// <param name="fields">names of the fields to insert</param>
        /// <param name="values">values for the fields </param>
        public bool Insert(string table, string fields, string values)
        {
            bool res = false;
            try
            {
                openConnection();
                //create query
                //Execute query
                string query = "INSERT INTO " + table + " (" + fields + ") VALUES " + "(" + values + ")";
            
                com = new MySqlCommand(query, con);
                com.ExecuteNonQuery();
                res = true;
            }
            catch (MySqlException sqlExcept)
            {
                msgError = "Error sql insert, with error:  " + sqlExcept.Message;
            }
            catch (Exception ex)
            {
                msgError = "Error to insert:  " + ex.Message;
            }
            finally
            {
                closeConnection();
            }
            return res;
        }

        //++++TODO++++ refactory at this query to create a prepared statement 
        /// <summary>
        /// search data 
        /// </summary>
        /// <param name="where">param for search</param>
        /// <param name="fields">names of the fields to insert</param>
        /// <param name="table">table to search</param>
        public List<object> SearchCli()
        {
            List<object> res = new List<object>();
            try
            {
                openConnection();
                string query = "SELECT clients.id, clients.name, clients.rfc, states.state, municipalities.municipality"
                                +" FROM clients inner join states "
                                +" on clients.fk_id_state = states.id inner join " 
                                +" municipalities on clients.fk_id_municipality = municipalities.id";
                com = new MySqlCommand(query, con);
                dr = com.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        //Array to fill the datagrid
                        object[] temp = new object[dr.FieldCount];
                        for (int i = 0; i < temp.Length; i++)
                        {
                            temp[i] = dr[i];
                        }
                        res.Add(temp);
                    }
                }
                else
                {
                    msgError = "No results were found";
                }
            }
            catch (MySqlException sqlExcept)
            {
                msgError = "Error SQL -SELECT *- data" + sqlExcept.Message;

            }
            catch (Exception ex)
            {
                msgError = "Error to show" + ex.Message;

            }
            finally
            {
                closeConnection();
            }
            //devolver lista
            return res;
        }

        //--------------------------------------------*********************************************----------------------------------------------------
        /// <summary>
        /// Update Data
        /// </summary>
        /// <param name="where">param for update</param>
        /// <param name="valueFields">valuesandfields of the fields to insert</param>
        /// <param name="table">table to search</param>
        public bool Update(string tabla, string valueFields, string where)
        {
            bool res = false;

            try
            {
                openConnection();
                string query = "UPDATE " + tabla + " SET " + valueFields + "  WHERE " + where + ";";
                com = new MySqlCommand(query, con);
                int resQuery = com.ExecuteNonQuery();
                //return results
                if (resQuery > 0)
                    res = true;
                else msgError = "There is no record with these data that is updated";
            }
            catch (MySqlException mysqlex)
            {
                msgError = "Error SQL to update. " + mysqlex.Message;
            }
            catch (Exception ex)
            {
                msgError = "Error to update. " + ex.Message;
            }
            finally
            {
                closeConnection();
            }
            return res;
        }

        //--------------------------------------------*********************************************----------------------------------------------------
        /// <summary>
        /// Delete Data
        /// </summary>
        /// <param name="where">param to delete</param>
        /// <param name="fields">fields to consult</param>
        /// <param name="table">table to consult</param>
        public List<object> Consult(string fields, string table, string where)
        {
            List<object> res = new List<object>();
            try
            {
                openConnection();
                string query = "SELECT " + fields + " FROM " + table + " WHERE " + where;
                com = new MySqlCommand(query, con);
                dr = com.ExecuteReader();
                //procces data to result
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        //temp array to fill
                        //add to the list
                        object[] temp = new object[dr.FieldCount];

                        //fill each field in array
                        for (int i = 0; i < temp.Length; i++)
                        {
                            temp[i] = dr[i];
                        }
                        //array to list
                        res.Add(temp);
                    }
                }
                else
                {
                    msgError = "There is no record with these data that is consulted";
                }
            }
            catch (MySqlException mysqlex)
            {
                msgError = "Error SQL to consult." + mysqlex.Message;
            }
            catch (Exception ex)
            {
                msgError = "Error to consult." + ex.Message;
            }
            finally { closeConnection(); }
            return res;
        }

        //--------------------------------------------*********************************************----------------------------------------------------
        /// <summary>
        /// Delete Data
        /// </summary>
        /// <param name="where">param to delete</param>
        /// <param name="table">table to search</param>
        public bool Delete(string tabla, string where)
        {
            bool res = false;
            try
            {
                openConnection();
                string query = "DELETE FROM " + tabla + "  WHERE " + where;
                com = new MySqlCommand(query, con);
                int resQuery = com.ExecuteNonQuery();
                //return results
                if (resQuery > 0)
                    res = true;
                else msgError = "There is no record with these data that is deleted";
            }
            catch (MySqlException mysqlex)
            {
                msgError = "Error SQL to deleted. " + mysqlex.Message;
            }
            catch (Exception ex)
            {
                msgError = "Error to deleted. " + ex.Message;
            }
            finally
            {
                closeConnection();
            }
            return res;
        }


    }
}
