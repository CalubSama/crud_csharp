﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CrudCatalogClients.logic;
using CrudCatalogClients.logic.Clients;

namespace CrudCatalogClients.presentation
{
    public partial class FrmClientsCatalog : Form
    {

        //var CRUD actions db
        Client cli = new Client();
        Municipality mun = new Municipality();
        State st = new State();
        int idClient = 0;
        string idMun = "";
        
        private DataSet DS = new DataSet();

        public FrmClientsCatalog()
        {
            InitializeComponent();
        }


        //--------------------------- My Methods ---------------------------
        private void ShowDataGridRegisters()
        {
            dgClients.DataSource = null;
            //clear DG 
            dgClients.Rows.Clear();
            //obj for search
            SearchParams param = new SearchParams();
            //Execute search and fill datagrid
            dgClients.DataSource = cli.SearchCli();
            //refresh dg
            dgClients.Update();
            dgClients.Refresh();
            dgClients.Visible = true;
        }
        private void ClearFields()
        {
            txtName.Text = "";
            txtRFC.Text = "";
            cboxState.SelectedItem = " ";
            cboxMunicipality.SelectedItem = " ";
        }

        //--------------------------- Winforms Methods ---------------------------
        private void btnAddClients_Click(object sender, EventArgs e)
        {
            Client addClient = new Client();
            addClient.Name = txtName.Text;
            addClient.RFC1 = txtRFC.Text;
            addClient.State = cboxState.SelectedIndex.ToString();
            addClient.Municipality = cboxMunicipality.SelectedIndex.ToString();

            if (addClient.Add())
            {
                ClearFields();
                ShowDataGridRegisters();
                MessageBox.Show("New Client registered");

            }
            else
            {
                MessageBox.Show("Error trying to save client " + Client.msgError);
            }
        }

        private void FrmClientsCatalog_Load(object sender, EventArgs e)
        {

            ShowDataGridRegisters();
            cboxState.DataSource = st.Search();
            cboxState.DisplayMember = "stat";
            cboxState.ValueMember = "id";
        }

        private void txtSearch_KeyPress(object sender, KeyPressEventArgs e)
        {
            //TODO implement method for search
        }

        private void dgClients_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                idClient = int.Parse(dgClients.Rows[e.RowIndex].Cells[0].Value.ToString());
                txtName.Text = dgClients.Rows[e.RowIndex].Cells[1].Value.ToString();
                txtRFC.Text = dgClients.Rows[e.RowIndex].Cells[2].Value.ToString();
                cboxState.SelectedItem = dgClients.Rows[e.RowIndex].Cells[3].Value.ToString();
                cboxMunicipality.SelectedItem = dgClients.Rows[e.RowIndex].Cells[3].Value.ToString();
            }
        }

        private void btnDeleteClients_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("You really want to delete this client?", "Delete Client", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                if (cli.Delete(idClient))
                {
                    MessageBox.Show("Client Deleted");

                }
                else MessageBox.Show("Error, Client was not eliminated");
            }
            ShowDataGridRegisters();
        }

        private void btnUpdateClients_Click(object sender, EventArgs e)
        {
            //Datos Esta tomando el ultimo dato y lo sustituye en el sig add.
            Client updateCli = new Client();
            List<UpdateData> data = new List<UpdateData>();

            data.Add(new UpdateData("name", txtName.Text));
            data.Add(new UpdateData("rfc", txtRFC.Text));
            data.Add(new UpdateData("fk_id_state", (cboxState.SelectedIndex + 1).ToString()));
            data.Add(new UpdateData("fk_id_municipality", (cboxMunicipality.SelectedValue).ToString()));

            if (updateCli.Update(data, idClient))
            {
                ClearFields();
                ShowDataGridRegisters();
                MessageBox.Show("client updated");
            }
            else MessageBox.Show("Error, client was not updated");

        }

        private void cboxState_SelectedIndexChanged(object sender, EventArgs e)
        {
                
                cboxMunicipality.DataSource = mun.Search(cboxState.SelectedIndex + 1);
                cboxMunicipality.DisplayMember = "municipa";
                cboxMunicipality.ValueMember = "Id";
        }

        private void cboxMunicipality_SelectedValueChanged(object sender, EventArgs e)
        {
            idMun = cboxMunicipality.SelectedValue.ToString();
        }
    }
}
